export const GoodsCategories = [
  {label: '营养类', value: 1},
  {label: '感官类', value: 2},
  {label: '皮肤类', value: 3},
  {label: '呼吸类', value: 4},
];

export const ArticleCategories = [
  {label: '港澳台动态', value: 1},
  {label: '国外近况', value: 2},
  {label: '新闻动态', value: 3},
  {label: '全国招商', value: 4},
  {label: '用户反馈', value: 5},
  {label: '品牌活动', value: 6},
];

export const GoodsTypes = [
  {label: '普通类', value: 1},
  {label: '可试用', value: 2}
]


export const ImgDomain = 'http://cardoo.qiniudn.com/';