import Vue from 'vue'
import AuthCache from '../cache/auth'
import { login } from '../api'

export default {
  namespaced: true,
  state: {
    ...AuthCache(),
    qiniuToken: ''
  },
  getters: {
    user: state => {
      const {nick, username, token} = state || {}
      console.log('state', state)
      return { 
        title: nick, 
        name: username, 
        token
      }
    }
  },
  mutations: {
    loginSuccess (state, payload) {
      const { $route } = payload;
      delete payload.$route;
      Object.keys(payload).forEach(key => {
        state[key] = payload[key]
      })
      $route.push('users')
    },
    signOutSuccess (state, payload) {
      const {resolve} = payload;
      Object.keys(state).forEach(key => {
        state[key] = null
      })
      resolve && resolve();
    },
    fetchTokenSuccess (state, payload) {
      const {result} = payload;
      if(typeof result === 'string'){
        state.qiniuToken = result
      }
    }
  },
  actions: {
    async login ({commit}, data) {
      const auth = AuthCache()
      const {$route} = data
      delete data.$route
      try {
        const res = await login(data)
        commit('loginSuccess', {...res, $route})
      }catch(err){
        console.log(err.message)
      }
    },
    signOut ({commit}, {resolve}) {
      AuthCache.clear()
      commit('signOutSuccess', {resolve})
    }
  }
}
