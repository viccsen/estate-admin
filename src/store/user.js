import request from '../utils/request'
import { fetchUsers, assessUser } from '../api/user'

export default {
  namespaced: true,
  state: {
    list: [
    ],
    total: 0,
  },
  getters: {
  },
  mutations: {
    fetchUsersSuccess (state, payload) {
      const { result } = payload
      console.log('result', result)
      result && Object.keys(result).forEach(key => {
        state[key] = result[key]
      })
    },
    toAssessUserSuccess (state, payload) {
      const { result } = payload
      state.item = result ? result.outFits : []
    },
  },
  actions: {
    async fetchUserList ({commit}, data) {
      try {
        const res = await fetchUsers(data)
        commit({type: 'fetchUsersSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
    async toAssessUser ({commit}, data) {
      try {
        const res = await assessUser(data)
        commit({type: 'toAssessUserSuccess', ...res})
      }catch(err){
        console.error(err.message)
      }
    },
  }
}
