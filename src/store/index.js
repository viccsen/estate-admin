import Vue from 'vue'
import Vuex from 'vuex'
import router from './router'
import auth from './auth'
import user from './user'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    router,
    auth,
    user
  }
})

export default store
