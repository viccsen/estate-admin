import request from '@/utils/request'

const path = '/api/1.0/wechat'

/**
 * 获取用户列表
 */

export const fetchUsers = data => request(path, {data: {s: 10, ...data}, method: 'GET'});

/**
 * 审核用户
 */

export const assessUser = data => {
    const { id } = data
    delete data.id
    return request(path + '/' + id, {data, method: 'PUT' });
}
