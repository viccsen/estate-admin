import request from '@/utils/request'
import authenticateCache from '../cache/auth';


/**
 * 七牛token
 */

// export const fetchQiniuToken = data =>  request('/admin/1.0/auth/biz/qiniu/token', {data, method: 'GET'})

/**
 * 登陆
 */

export const login = data => request('/api/1.0/adminLogin/1', {data, method: 'GET'}, authenticateCache)
