
# About

此项目是 vue + element-ui 构建的后台管理系统


# 说明

>  如果对您对此项目有兴趣，可以点 "Star" 支持一下 谢谢！ ^_^

>  或者您可以 "follow" 一下，我会不断开源更多的有趣的项目

>  nodejs 10.7.0



## 技术栈

vue2 + vuex + vue-router + webpack + ES6/7 + less + element-ui


## 项目运行


```
npm install

npm run dev (访问线上后台系统)

npm run local (访问本地后台系统，需运行node-elm后台系统)


访问: http://localhost:8002

```

# 功能列表

- [x] 登陆/注销 -- 完成 
- [x] 用户审核 -- 完成
